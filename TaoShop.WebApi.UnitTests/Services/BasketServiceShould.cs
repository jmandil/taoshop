﻿using System.Collections.Generic;
using Moq;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;
using TaoShop.WebApi.Services;

namespace TaoShop.WebApi.UnitTests.Services
{
    [TestFixture]
    public class BasketServiceShould
    {
        [Test]
        public void AddProductToBasket()
        {
            var products = new Mock<Products>();
            var product = new Product
            {
                Id = 1,
                Name = "test",
                Description = "description",
                Stock = 10
            };
            var productsInBasket = new List<Product>();
            var singleProduct = new Product
            {
                Id = 1,
                Name = "test",
                Description = "description",
                Stock = 1
            };
            //productsInBasket.Add(singleProduct);
            products.Setup(x => x.FindProductBy(product.Id)).Returns(product);
            products.Setup(x => x.GetBasket()).Returns(productsInBasket);
            var basketService = new BasketService(products.Object);

            var basketContents = basketService.Add(product);

            products.Verify(x => x.AddToBasket(It.IsAny<Product>()), Times.Once);
        }

        [Test]
        public void AddProductAlreadyBasketAndIncreaseQuantity()
        {
            var products = new Mock<Products>();
            var product = new Product
            {
                Id = 1,
                Name = "test",
                Description = "description",
                Stock = 10
            };
            var productsInBasket = new List<Product>();
            var singleProduct = new Product
            {
                Id = 1,
                Name = "test",
                Description = "description",
                Stock = 1
            };
            productsInBasket.Add(singleProduct);
            products.Setup(x => x.FindProductBy(product.Id)).Returns(product);
            products.Setup(x => x.GetBasket()).Returns(productsInBasket);
            var basketService = new BasketService(products.Object);

            var basketContents = basketService.Add(product);

            products.Verify(x => x.AddToBasket(It.IsAny<Product>()), Times.Never);
            basketContents.Products.Count.ShouldBe(1);
            basketContents.NumberOfItemsInBasket().ShouldBe(2);
        }

        [Test]
        public void DoNotAddProductToBasketWhenStockNotAvailable()
        {
            var products = new Mock<Products>();
            var product = new Product
            {
                Id = 1,
                Name = "test",
                Description = "description",
                Stock = 0
            };
            var productsInBasket = new List<Product>();

            products.Setup(x => x.FindProductBy(product.Id)).Returns(product);
            products.Setup(x => x.GetBasket()).Returns(productsInBasket);
            var basketService = new BasketService(products.Object);

            var basketContents = basketService.Add(product);

            products.Verify(x => x.Update(It.IsAny<Product>()), Times.Never);
            products.Verify(x => x.AddToBasket(It.IsAny<Product>()), Times.Never);
            basketContents.Products.Count.ShouldBe(0);
        }
    }
}

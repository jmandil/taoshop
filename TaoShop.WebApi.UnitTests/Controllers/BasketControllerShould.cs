﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Controllers;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;
using TaoShop.WebApi.Services;

namespace TaoShop.WebApi.UnitTests.Controllers
{
    [TestFixture]
    public class BasketControllerShould
    {
        private Mock<Products> _mockProducts;
        private Mock<BasketService> _mockBasketService;
        
        private BasketController _basketController;

        [SetUp]
        public void SetUp()
        {
            _mockProducts = new Mock<Products>();
            _mockBasketService = new Mock<BasketService>(_mockProducts.Object);
            _basketController = new BasketController(_mockBasketService.Object);
        }

        [Test]
        public void AddItemToBasket()
        {
            var expectedProduct = new Product {Id = 1, Name = "test", Description = "description", Stock = 1};
            const int identifier = 1;
            _mockProducts.Setup(x => x.FindProductBy(identifier)).Returns(expectedProduct);
            _mockBasketService.Setup(b => b.AddProductBy(expectedProduct.Id)).Returns(new BasketContents { Products = new List<Product> {expectedProduct} });

            var result = _basketController.AddProduct(identifier);

            result.Products.ToList().Count.ShouldBe(1);
        }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Controllers;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.WebApi.UnitTests.Controllers
{
    [TestFixture]
    public class ProductsControllerShould
    {
        private Mock<Products> _products;

        [Test]
        public void ReturnAListOfProducts()
        {
            _products = new Mock<Products>();
            var items = new List<Product>
            {
                new Product { Id = 1, Name = "Item1", Description = "ItemDescr1", Stock = 40},
                new Product { Id = 2, Name = "Item2", Description = "ItemDescr2", Stock = 0},
                new Product { Id = 3, Name = "Item3", Description = "ItemDescr3", Stock = 3},
                new Product { Id = 4, Name = "Item4", Description = "ItemDescr4", Stock = 14},
                new Product { Id = 5, Name = "Item5", Description = "ItemDescr5", Stock = 12},
            };
            _products.Setup(i => i.GetProducts()).Returns(items);
            var productsController = new ProductsController(_products.Object);

            var results = productsController.Get().ToList();

            results.Count.ShouldBe(5);
            foreach (var result in results)
            {
                var item = items.First(x => x.Id == result.Id);

                item.ShouldNotBeNull();
                item.Name.ShouldBe(result.Name);
                item.Description.ShouldBe(result.Description);
                item.Stock.ShouldBe(result.Stock);
            }    
        }
    }
}

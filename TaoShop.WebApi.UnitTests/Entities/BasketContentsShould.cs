﻿using System.Linq;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Entities;

namespace TaoShop.WebApi.UnitTests.Entities
{
    [TestFixture]
    public class BasketContentsShould
    {
        [Test]
        public void AddProductToBasket()
        {
            var expectedProduct = new Product {Id = 1, Name = "Test", Description = "Description", Stock =  11};
            var basketContents =  new BasketContents();

            basketContents.AddItemToBasket(expectedProduct);

            basketContents.Products.Count.ShouldBe(1);
            var actual = basketContents.Products.FirstOrDefault(x => x.Id == expectedProduct.Id);
            actual.ShouldNotBeNull();
            actual.Name.ShouldBe(expectedProduct.Name);
            actual.Description.ShouldBe(expectedProduct.Description);
            actual.Stock.ShouldBe(expectedProduct.Stock);

        }
    }
}

# README #

### What is this repository for? ###

* TaoShop - is a simple ecommerce site demoing SPA and WebApi architecture in .Net.
* Version 0.0.0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Visual Studio 2015.
* Run NUnit tests using ReSharper (optional) or NUnit Runner.
* Dependencies are from NuGet. Just building the solution should download the dependecies.

### Contribution guidelines ###

* Use best TDD practices.
* Owner will code review.

### Who do I talk to? ###

* Repo owner
﻿using System.Collections.Generic;
using TaoShop.WebApi.Entities;

namespace TaoShop.WebApi.Repository
{
    public interface Products
    {
        IList<Product> GetProducts();
        void AddToBasket(Product product);
        Product FindProductBy(int id);
        void Update(Product product);
        IList<Product> GetBasket();
    }
}
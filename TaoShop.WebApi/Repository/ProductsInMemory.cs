﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaoShop.WebApi.Entities;

namespace TaoShop.WebApi.Repository
{
    public class ProductsInMemory : Products
    {
        private readonly List<Product> _products;
        private readonly List<Product> _basket;

        public ProductsInMemory()
        {
            _products = new List<Product>
            {
                new Product { Id = 1, Name = "Something", Description = "Nice", Stock = 5 }
            };
            _basket = new List<Product>();
        }
        public IList<Product> GetProducts()
        {
            return _products;
        }

        public IList<Product> GetBasket()
        {
            return _basket;
        }

        public void AddToBasket(Product product)
        {
            var productCopy = new Product
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Stock = 1
            };
            _basket.Add(productCopy);
        }

        public Product FindProductBy(int identifier)
        {
            return null;
        }

        public void DeductQuantityFrom(Product product)
        {
            throw new NotImplementedException();
        }

        public void Update(Product product)
        {
            var productToUpdate = _products.First(x => x.Id == product.Id);
            productToUpdate.Name = product.Name;
            productToUpdate.Description = product.Description;
            productToUpdate.Stock = product.Stock;
        }
    }
}
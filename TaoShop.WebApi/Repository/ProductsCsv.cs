﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using CsvHelper;
using TaoShop.WebApi.Entities;

namespace TaoShop.WebApi.Repository
{
    public class ProductsCsv : Products
    {
        private const string CsvFile = "App_Data\\example_data.csv";
        public IList<Product> GetProducts()
        {
            IList<Product> products;
            var path = Path.Combine(GetAssemblyPath(), CsvFile);

            using (var textReader = File.OpenText(path))
            {
                var csv = new CsvReader(textReader);
                csv.Configuration.HasHeaderRecord = true;
                csv.Configuration.IgnoreHeaderWhiteSpace = true;
                products = csv.GetRecords<Product>().ToList();
            }

            return products;
        }

        public void AddToBasket(Product product)
        {
            throw new System.NotImplementedException();
        }

        public Product FindProductBy(int identifier)
        {
            throw new System.NotImplementedException();
        }

        public void Update(Product product)
        {
            throw new System.NotImplementedException();
        }

        public IList<Product> GetBasket()
        {
            throw new System.NotImplementedException();
        }

        private static string GetAssemblyPath()
        {
            return Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase).Replace(@"file:\", string.Empty);
        }

    }
}

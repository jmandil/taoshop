using System.Collections.Generic;
using System.Linq;

namespace TaoShop.WebApi.Entities
{
    public class BasketContents
    {
        public List<Product> Products { get; set; }

        public BasketContents()
        {
            Products = new List<Product>();
        }

        public void AddItemToBasket(Product product)
        {
            Products.Add(product);
        }

        public int NumberOfItemsInBasket()
        {
            return Products.Sum(x => x.Stock);
        }
    }
}
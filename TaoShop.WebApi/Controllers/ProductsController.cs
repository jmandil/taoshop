﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Cors;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.WebApi.Controllers
{
    [EnableCorsAttribute("http://localhost:38895", "*", "*")]
    public class ProductsController : ApiController
    {
        private readonly Products _products;

        public ProductsController(Products products)
        {
            _products = products;
        }

        protected ProductsController() : this (new ProductsCsv())
        {
        }

        public IEnumerable<Product> Get()
        {
            return _products.GetProducts();
        }
    }
}
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Services;

namespace TaoShop.WebApi.Controllers
{
    public class BasketController
    {
        private readonly BasketService _basketService;

        public BasketController(BasketService basketService)
        {
            _basketService = basketService;
        }

        public BasketContents AddProduct(int identifier)
        {
            return _basketService.AddProductBy(identifier);
        }
    }
}
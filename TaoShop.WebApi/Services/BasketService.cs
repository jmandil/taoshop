﻿using System.Linq;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.WebApi.Services
{
    public class BasketService
    {
        private readonly Products _products;


        public BasketService(Products products)
        {
            _products = products;
        }

        public virtual BasketContents Add(Product product)
        {
            if (CheckIfProductIsAvailable(product))
            {
                var productAlreadyInBasket = _products.GetBasket().FirstOrDefault(x => x.Id == product.Id);

                if (productAlreadyInBasket == null)
                {
                    var productToAdd = new Product
                    {
                        Id = product.Id,
                        Name = product.Name,
                        Description = product.Description,
                        Stock = 1
                    };
                    _products.AddToBasket(productToAdd);
                }
                else
                {
                    productAlreadyInBasket.Stock++;
                }
            }

            var basketContents = new BasketContents
            {
                Products = _products.GetBasket().ToList()
            };

            return basketContents;
        }

        public virtual BasketContents AddProductBy(int identifier)
        {
            return Add(_products.FindProductBy(identifier));
        }

        private static bool CheckIfProductIsAvailable(Product product)
        {
            return product.Stock > 0;  
        }
    }
}
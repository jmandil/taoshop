using System.Linq;
using Moq;
using Shouldly;
using TaoShop.WebApi.Controllers;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Services;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace TaoShop.Specs.AsAShopper
{
    [Binding]
    public class AddItemToBasketSteps
    {
        private static Mock<BasketService> _basketService;
        private readonly BasketController _basketController;
        private readonly BasketContents _setBasketContents;
        private BasketContents _result;
        private readonly ProductContext _productContext;
        

        public AddItemToBasketSteps(ProductContext productContext)
        {
            _basketService = new Mock<BasketService>();
            _productContext = productContext;
            _basketController = new BasketController(_basketService.Object);
            _setBasketContents = new BasketContents();
        }

        [When(@"I add item with identifier (.*) to my basket")]
        public void WhenIAddItemWithIdentifierToMyBasket(int identifier)
        {
            _setBasketContents.Products = _productContext.Products.Where(x => x.Identifier == identifier).ToList();
            
            _basketService.Setup(i => i.AddProductBy(identifier)).Returns(_setBasketContents);
            
            _result = _basketController.AddProduct(identifier);
        }

        [Then(@"the main screen will show the basket")]
        public void ThenTheMainScreenWillShowTheBasket(Table table)
        {
            _result.ShouldNotBeNull();
            _result.Products.ShouldNotBeNull();


            //var expectedProducts = table.CreateSet<Product>().ToList();

            //foreach (var expectedProduct in expectedProducts)
            //{
            //    var actualProduct = _result.Products.FirstOrDefault(x => x.Identifier == expectedProduct.Identifier);
            //    actualProduct.ShouldNotBeNull();
            //    actualProduct.Name.ShouldBe(expectedProduct.Name);
            //    actualProduct.Description.ShouldBe(expectedProduct.Description);
            //    actualProduct.Quantity.ShouldBe(expectedProduct.Quantity);
            //}
        }

        [Then(@"the basket will show (.*) tems added")]
        public void ThenTheBasketWillShowTemsAdded(int numOfItems)
        {
            _result.ShouldNotBeNull();
            _result.NumberOfItemsInBasket().ShouldBe(numOfItems);
        }
    }
}

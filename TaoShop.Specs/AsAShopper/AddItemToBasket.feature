﻿Feature: AddItemToBasket
	In order to purchase items
	As a shopper I want to add an item to my basket
	so that I can purchase it later.

Background: 
	Given there is a list of items in storage
		| Identifier | Name | Description               | Quantity |
		| 1          | Vase | 16th Century chinese vase | 3        |
		| 2          | Bowl | 60's bowl                 | 1        |
		| 3          | Pen  | Rare Parker Pen           | 0        |
		
Scenario: Add an item to basket
	Given I visit the main page
	When I add item with identifier 1 to my basket
	When I add item with identifier 1 to my basket
	And I add item with identifier 2 to my basket
	Then the basket will show 3 tems added

Scenario: Cannot add an item to basket when stock availability is zero
	Given I visit the main page
	When I add item with identifier 3 to my basket
	Then the basket will show 0 tems added
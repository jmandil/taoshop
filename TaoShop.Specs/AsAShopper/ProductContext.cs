﻿using System.Collections.Generic;
using TaoShop.WebApi.Entities;

namespace TaoShop.Specs.AsAShopper
{
    public class ProductContext
    {
        public List<Product> Products { get; set; }

        public ProductContext()
        {
            Products = new List<Product>();
        }
    }
}

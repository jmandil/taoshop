﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.Specs.AsAShopper
{
    public class FakeProductRepository : IProducts
    {
        public IList<Product> GetProducts()
        {
            throw new NotImplementedException();
        }

        public void AddToBasket(Product product)
        {
            throw new NotImplementedException();
        }

        public Product FindProductBy(int identifier)
        {
            throw new NotImplementedException();
        }

        public void Update(Product product)
        {
            throw new NotImplementedException();
        }

        public IList<Product> GetBasket()
        {
            throw new NotImplementedException();
        }
    }
}

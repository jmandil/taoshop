﻿using System.Collections.Generic;
using System.Linq;
using Moq;
using Shouldly;
using TaoShop.WebApi.Controllers;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace TaoShop.Specs.AsAShopper
{
    [Binding]
    public class ViewAllItemsICanPurchaseSteps
    {
        private readonly ProductContext _productContext;
        private Mock<Products> _products;
        protected ProductsController ProductsController;
        protected IEnumerable<Product> Result;

        public ViewAllItemsICanPurchaseSteps(ProductContext productContext)
        {
            _productContext = productContext;
        }

        [Given(@"there is a list of items in storage")]
        public void GivenThereIsAListOfItemsInStorage(Table table)
        {
            _products = new Mock<Products>();

            _productContext.Products = table.CreateSet<Product>().ToList();

            _products.Setup(i => i.GetProducts()).Returns(_productContext.Products);
        }

        [Given(@"I visit the main page")]
        public void GivenIVisitTheMainPage()
        {
            ProductsController = new ProductsController(_products.Object);

        }

        [When(@"a request for a list of items is made")]
        public void WhenARequestForAListOfItemsIsMade()
        {
            Result = ProductsController.Get();
        }

        [Then(@"the result should show (.*) items on the page")]
        public void ThenTheResultShouldShowItemsOnThePage(int numOfItems)
        {
            Result.ToList().Count.ShouldBe(numOfItems);
        }
        
        [Then(@"it displays the items")]
        public void ThenItDisplaysTheItems(Table table)
        {
            foreach (var row in table.Rows)
            {
                var item = Result.First(x => x.Id == int.Parse(row["Id"]));
                item.Name.ShouldBe(row["Name"]);
                item.Description.ShouldBe(row["Description"]);
                item.Stock.ShouldBe(int.Parse(row["Stock"]));
                item.Price.ShouldBe(decimal.Parse(row["Price"]));
            }
        }
    }
}

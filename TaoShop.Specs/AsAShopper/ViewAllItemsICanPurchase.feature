﻿Feature: ViewAllItemsICanPurchase
	As a shopper
	I want to be able to see a list of all possible items available
	so that I can determine what I can buy

Background: 
	Given there is a list of items in storage
		| Id | Name         | Description | Stock | Price |
		| 1  | Banana       | Fruit       | 3     | 1.23  |
		| 2  | Carrots      | Vegatable   | 1     | 0.50  |
		| 3  | Ornage Juice | Drink       | 4     | 2.00  |

Scenario: List all items on product page
	Given I visit the main page
	When a request for a list of items is made
	Then the result should show 3 items on the page
		And it displays the items
		| Id | Name         | Description | Stock | Price |
		| 1  | Banana       | Fruit       | 3     | 1.23  |
		| 2  | Carrots      | Vegatable   | 1     | 0.50  |
		| 3  | Ornage Juice | Drink       | 4     | 2.00  |

﻿
"use strict";
angular
    .module("taoShopApp")
    .controller("ProductListCtrl"
    , ["productResource",
        ProductListCtrl]);

function ProductListCtrl(productResource) {
    var vm = this;

    productResource.query(function(data) {
        vm.products = data;
    });
}
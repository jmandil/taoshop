﻿using System.Linq;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.AdapterTests.Repository
{
    [TestFixture]
    public class ProductsShould
    {
        [Test]
        public void AddItemToUsersBasket()
        {
            var products = new ProductsInMemory();
            var product = new Product
            {
                Id = 1,
                Name = "Product1",
                Description = "Decr",
                Stock = 12
            };

            products.AddToBasket(product);

            var basket = products.GetBasket();
            basket.ShouldNotBeNull();
            basket.Count.ShouldBe(1);
            basket.First(x=>x.Id == product.Id).Stock.ShouldBe(1);
        }
    }    
}

using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using NUnit.Framework;
using Shouldly;
using TaoShop.WebApi.Entities;
using TaoShop.WebApi.Repository;

namespace TaoShop.AdapterTests.Repository
{
    [TestFixture]
    public class ProductsCsvShould
    {
        [Test]
        public void GetAllProductsFromCsvFile()
        {
            var expectedProducts = new List<Product>
            {
                new Product {Id = 1, Name = "Apples", Description = "Fruit", Stock = 5, Price = 2.5m},
                new Product {Id = 2, Name = "Bread", Description = "Loaf", Stock = 10, Price = 1.35m},
                new Product {Id = 3, Name = "Oranges", Description = "Fruit", Stock = 10, Price = 2.99m},
                new Product {Id = 4, Name = "Milk", Description = "Milk", Stock = 3, Price = 2.07m},
                new Product {Id = 5, Name = "Chocolate", Description = "Bars", Stock = 20, Price = 1.79m}
            };
            var productsCsv = new ProductsCsv();

            var products = productsCsv.GetProducts();

            products.Count.ShouldBe(5);
            foreach (var expectedProduct in expectedProducts)
            {
                var first = expectedProducts.First(x => x.Id == expectedProduct.Id);
                first.ShouldNotBeNull();
                first.Name.ShouldBe(expectedProduct.Name);
                first.Description.ShouldBe(expectedProduct.Description);
                first.Stock.ShouldBe(expectedProduct.Stock);
                first.Price.ShouldBe(expectedProduct.Price);
            }
        }

        
    }
}